<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keranjang extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_member','pesan'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('id_member')){
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Harap Login terlebih dahulu'));
      redirect('/');
    }
  }

  function index()
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }

    $id_member = $this->session->userdata('id_member');
    $data['chart'] =  $this->M_member->getCartItem($id_member)->result();
    $data['paketChart'] =  $this->M_member->getPaketCartItem($id_member)->result();
    
    if(count($data['chart'])!=0){
      $data['id_transaksi'] = $data['chart'][0]->id_transaksi;
      $total_transaksi = $data['chart'][0]->total_harga;
    }else{
      $data['id_transaksi'] = 0;
      $total_transaksi = 0;
    }

    if(count($data['paketChart'])!=0){
      $data['id_transaksi_paket'] = $data['paketChart'][0]->id_transaksi_paket;
      $total_transaksi_paket = $data['paketChart'][0]->total_harga;
    }else{
      $data['id_transaksi_paket'] = 0;
      $total_transaksi_paket = 0;
    }

    $data['total'] = $total_transaksi+$total_transaksi_paket;

    $this->load->view('header', $data);
    $this->load->view('keranjang',$data);
    $this->load->view('footer');
  }

  function checkout(){
    $id_member = $this->session->userdata('id_member');
    $bank_tujuan = $this->input->post('bank_tujuan');
    $pemilik_rekening = $this->input->post('pemilik_rekening');
    $nomor_rekening = $this->input->post('nomor_rekening');
    $id_transaksi = $this->input->post('id_transaksi');
    $id_transaksi_paket = $this->input->post('id_transaksi_paket');

    $status_transaksi = [
      'status_transaksi'=>1
    ];

    $data=[
      'id_member'=>$id_member,
      'bank_tujuan'=>$bank_tujuan,
      'pemilik_rekening'=>$pemilik_rekening,
      'nomor_rekening'=>$nomor_rekening,
      'id_transaksi'=>$id_transaksi,
      'id_transaksi_paket'=>$id_transaksi_paket,
      'status_pembayaran'=>0,
      'tgl_pembayaran'=>date("Y-m-d")
    ];

    $this->M_member->checkout($data);
    $this->M_member->updateStatusTransaksi($id_transaksi,$status_transaksi);
    $this->M_member->updateStatusTransaksiPaket($id_transaksi_paket,$status_transaksi);
    $this->session->set_userdata('chart',0);
    $this->session->set_flashdata('memberCheckout', $this->pesan->danger('Anda memiliki waktu 1 hari untuk upload bukti pembayaran'));
    redirect('/');
  }

  function lihatItem($id_item, $id_paket){
    if ($id_item != 0) {
      $data = $this->M_member->getCartItemById($id_item)->result();  
    } else {
      $data = $this->M_member->getPaketCartItemById($id_paket)->result();
    }
    echo json_encode($data);
  }
}
