<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_member','M_admin','pesan'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('id_member')){
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Harap Login terlebih dahulu'));
      redirect('/');
    }
  }

  function index($id_item,$tipe)
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }

    $data['tipe'] =  $tipe;
    $data['id_item'] = $id_item;
    if($tipe==1){
      $data['paket'] = $this->M_member->getPaketById($id_item)->row();
      $data['paket_item'] = $this->M_member->getDetailPaket($id_item);
      $this->load->view('header', $data);
      $this->load->view('pesan_paket',$data);
      $this->load->view('footer');
    }
    else{
      $data['item'] = $this->M_member->getItemById($id_item)->row();
      $this->load->view('header', $data);
      $this->load->view('pesan',$data);
      $this->load->view('footer');
    }
  }

  function masukChart(){

    $id_member = $this->session->userdata('id_member');
    $tgl_upacara = $this->input->post('tgl_upacara');
    $qty = $this->input->post('qty_item');
    $pesan_detail = $this->input->post('pesan_detail');
    $id_item = $this->input->post('id_item');
    $harga = $this->input->post('harga');
    $total_harga = $harga*$qty;
    $date = DateTime::createFromFormat('m/d/Y', $tgl_upacara);

    $cek = $this->M_member->cekOldChart($id_member)->num_rows();

    if($cek==0){
      $newCart = [
        'id_member'=>$id_member,
        'tgl_transaksi'=>date("Y-m-d"),
        'status_transaksi'=>0,
        'total_harga'=>$total_harga
      ];
      $this->M_member->newCart($newCart);

      $cart = $this->M_member->cekOldChart($id_member)->row();
      $id_transaksi = $cart->id_transaksi;

      $data=[
        'tgl_upacara'=>$date->format('Y-m-d'),
        'qty_item'=>$qty,
        'id_item'=>$id_item,
        'harga'=>$harga,
        'id_transaksi'=>$id_transaksi
      ];
      $this->M_member->newDetailTransaksi($data);
      $this->session->set_userdata('chart',$this->session->userdata('chart')+1);
      redirect(site_url());
    }
    else{
      $cart = $this->M_member->cekOldChart($id_member)->row();
      $id_transaksi = $cart->id_transaksi;
      $total_sebelum = $cart->total_harga;
      $total_baru = $total_harga+$total_sebelum;

      $data=[
        'tgl_upacara'=>$date->format('Y-m-d'),
        'qty_item'=>$qty,
        'id_item'=>$id_item,
        'harga'=>$harga,
        'id_transaksi'=>$id_transaksi
      ];

      $data2=[
        'total_harga'=>$total_baru
      ];

      $this->M_member->updateTotalHarga($id_transaksi,$data2);
      $this->M_member->newDetailTransaksi($data);
      $this->session->set_userdata('chart',$this->session->userdata('chart')+1);
      redirect(site_url());
    }

  }

  function masukChartPaket(){

    $id_member = $this->session->userdata('id_member');
    $tgl_upacara = $this->input->post('tgl_upacara');
    $qty = 1;
    $pesan_detail = $this->input->post('pesan_detail');
    $id_item = $this->input->post('id_item');
    $harga = $this->input->post('harga');
    $date = DateTime::createFromFormat('Y-m-d', $tgl_upacara);
    $cek = $this->M_member->cekOldPaketChart($id_member)->num_rows();
    $total_harga = $harga*$qty;
    if($cek==0){
      $newCart = [
        'id_member'=>$id_member,
        'tgl_transaksi'=>date("Y-m-d"),
        'status_transaksi'=>0,
        'total_harga'=>$total_harga
      ];
      $this->M_member->newPaketCart($newCart);

      $cart = $this->M_member->cekOldPaketChart($id_member)->row();
      $id_transaksi = $cart->id_transaksi_paket;

      $data=[
        'tgl_upacara'=>date('Y-m-d', strtotime($tgl_upacara)),
        'id_item'=>$id_item,
        'harga'=>$harga,
        'id_transaksi_paket'=>$id_transaksi
      ];

      $this->M_member->newDetailPaketTransaksi($data);
      $this->session->set_userdata('chart',$this->session->userdata('chart')+1);
     
    }
    else{
      $cart = $this->M_member->cekOldPaketChart($id_member)->row();
      $id_transaksi = $cart->id_transaksi_paket;
      $total_sebelum = $cart->total_harga;
      $total_baru = $total_harga+$total_sebelum;

      $data=[
        'tgl_upacara'=>date('Y-m-d', strtotime($tgl_upacara)),
        'id_item'=>$id_item,
        'harga'=>$harga,
        'id_transaksi_paket'=>$id_transaksi
      ];

      $data2=[
        'total_harga'=>$total_baru
      ];

      $this->M_member->updateTotalHargaPaket($id_transaksi,$data2);
      $this->M_member->newDetailPaketTransaksi($data);
      $this->session->set_userdata('chart',$this->session->userdata('chart')+1);
     
    }

     //item
    $item_paket = $this->M_admin->item_paket($id_item)->result();
    foreach ($item_paket as $id) {


     $datax = [
        'id_transaksi_paket'=>$id_transaksi,
        'id_item'=>$id->item_id,
        'tgl_upacara'=>date('Y-m-d', strtotime($tgl_upacara)),
      ];
      $this->M_member->newPaketItem($datax);
      

       }



    redirect(site_url());

  }
}
