<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_member','pesan'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('id_member')){
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Harap Login terlebih dahulu'));
      redirect('/');
    }
  }

  function index()
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $id_member = $this->session->userdata('id_member');
    $data['pembayaran'] = $this->M_member->getPembayaran($id_member)->result();
    $this->load->view('header', $data);
    $this->load->view('pembayaran',$data);
    $this->load->view('footer');
  }

  function konfirmasi(){
    $id_pembayaran = $this->input->post('id_pembayaran');
    $tmp = explode(".", $_FILES['bukti_pembayaran']['name']);
    $ext = end($tmp);
    $gambar=time().'.'.$ext;
    $config['upload_path']   = './assets/images/bukti/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = 10000000;
    $config['max_width']     = 2000;
    $config['max_height']    = 2000;
    $config['file_name']   = $gambar;
    $config['overwrite'] = TRUE;

    $data=[
      'pemilik_rekening' => $this->input->post('pemilik_rekening'),
      'nomor_rekening' => $this->input->post('nomor_rekening'),
      'bukti_pembayaran'=>$gambar,
      'status_pembayaran'=>1
    ];

    $this->upload->initialize($config);
    $this->M_member->upload_bukti($data,$id_pembayaran);
    $upload=$this->upload->do_upload('bukti_pembayaran');
    $this->session->set_flashdata('bukti_pembayaran', $this->pesan->sukses('Bukti berhasil diupload. Pengiriman produk yang anda pesan akan kami konfirmasi sesuai dengan nomor telepon yang anda masukkan'));
    redirect('/pembayaran');
  }
}
