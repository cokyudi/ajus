<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_member','pesan'));
    if(!$this->session->userdata('id_member')){
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Harap Login terlebih dahulu'));
      redirect('/');
    }
  }

  function index()
  {
    $id_member = $this->session->userdata('id_member');
    $data['user'] = $this->session->userdata();
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $id_member = $this->session->userdata('id_member');
    $this->load->view('header', $data);
    $this->load->view('profil',$data);
    $this->load->view('footer');
  }

  function update() {
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $nama_member = $this->input->post('nama_member');
    $telp = $this->input->post('telp');
    $alamat = $this->input->post('alamat');
    $kota = $this->input->post('kota');
    $kode_pos = $this->input->post('kode_pos');

    $data= [
      'email'=>$email,
      'nama_member'=>$nama_member,
      'telp'=>$telp,
      'alamat'=>$alamat,
      'kota'=>$kota,
      'kode_pos'=>$kode_pos
    ];
    $register = $this->M_member->updateMember($data);

    $this->session->set_userdata('nama_member',$nama_member);
    $this->session->set_userdata('kota',$kota);
    $this->session->set_userdata('telp',$telp);
    $this->session->set_userdata('kode_pos',$kode_pos);
    $this->session->set_userdata('alamat',$alamat);

    redirect(site_url('Profil'));
  }
}
