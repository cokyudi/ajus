<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upacara extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $data['upacara'] = $this->M_admin->get_all_upacara()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/upacara',$data);
  }

  function ajax_get_upacara(){
    $id_upacara = $this->input->post('id_upacara');
    $data = $this->M_admin->ajax_get_upacara($id_upacara);
    echo json_encode($data[0]);
  }

  function add_upacara(){
    $data = $this->M_admin->add_upacara();

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Upacara baru telah ditambahkan!');
    }
    else if($data=='upacara_sama'){
      $this->session->set_flashdata('error', 'Upacara yang dimasukkan telah terdaftar sebelumnya!');
    }
    redirect("Admin/Upacara");
  }

  function update_upacara(){
    $data = $this->M_admin->edit_upacara();
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Upacara telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit Upacara!');
    }
    redirect("Admin/Upacara");
  }

  function delete_upacara($id_upacara){
    $data = $this->M_admin->delete_upacara($id_upacara);
    if ($data) {
      $this->session->set_flashdata('success', 'Upacara telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus Upacara!');
    }
    redirect("Admin/Upacara");
  }

}
