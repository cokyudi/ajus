<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

   function index(){
    $role = $this->session->userdata('admin_role');
    $data['paket'] = $this->M_admin->get_all_paket('promo')->result();
    $data['paket2'] = $this->M_admin->get_all_paket('tidak promo')->result();
    $data['upacara'] = $this->M_admin->get_all_upacara()->result();
    $data['item'] = $this->M_admin->get_all_item('promo')->result();
     $data['item2'] = $this->M_admin->get_all_item('tidak promo')->result();


    $this->load->view('admin/head');
    $this->load->view('admin/promo',$data);
  }

  function add_promo(){
    $id_paket = $this->input->post('id_paket');

    $update = [
      'harga_paket'=>$this->input->post('harga_paket'),
      'status'=>'promo'
    ];
    $data = $this->M_admin->edit_paket($update,$id_paket);

    if ($data=='berhasil') {
      $this->session->set_flashdata('paket', 'Paket Promo telah ditambahkan!');
    }
     else {
      $this->session->set_flashdata('paket', 'Gagal menambah paket promo!');
    }

   
   redirect("Admin/Promo");
  }

  function delete_promo(){
    $id_paket = $this->input->post('id_paket');

    $update = [
      'harga_paket'=>$this->input->post('harga_paket'),
      'status'=>'tidak promo'
    ];
    $data = $this->M_admin->edit_paket($update,$id_paket);

    if ($data=='berhasil') {
      $this->session->set_flashdata('paket', 'Paket Promo telah dihapus!');
    }
     else {
      $this->session->set_flashdata('paket', 'Gagal menghapus paket promo!');
    }
     
   redirect("Admin/Promo");
  }

   function add_promo_item(){
    $id_item = $this->input->post('id_item');

    $update = [
      'harga_jual'=>$this->input->post('harga_item'),
      'status'=>'promo'
    ];
    $data = $this->M_admin->edit_item($update,$id_item);

    if ($data=='berhasil') {
      $this->session->set_flashdata('item', 'Item Promo telah ditambahkan!');
    }
     else {
      $this->session->set_flashdata('item', 'Gagal menambah paket promo!');
    }
   redirect("Admin/Promo");
  }

  function delete_promo_item(){
    $id_item = $this->input->post('id_item');

    $update = [
      'harga_jual'=>$this->input->post('harga_item'),
      'status'=>'tidak promo'
    ];
    $data = $this->M_admin->edit_item($update,$id_item);

    if ($data=='berhasil') {
      $this->session->set_flashdata('item', 'Item Promo telah dihapus!');
    }
     else {
      $this->session->set_flashdata('item', 'Gagal menghapus item promo!');
    }

   redirect("Admin/Promo");
  }

}