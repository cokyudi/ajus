<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $data['member'] = $this->M_admin->get_all_member()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/member',$data);
  }

  function ajax_get_member(){
    $id_member = $this->input->post('id_member');
    $data = $this->M_admin->ajax_get_member($id_member);
    echo json_encode($data[0]);
  }

  function add_member(){
    $data = $this->M_admin->add_member();

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Member baru telah ditambahkan!');
    }
    else if($data=='email_sama'){
      $this->session->set_flashdata('error', 'Email yang dimasukkan telah terdaftar sebelumnya!');
    }
    else if($data=='password_beda'){
      $this->session->set_flashdata('error', 'Password yang dimasukkan tidak sama!');
    }
    redirect("Admin/Member");
  }

  function update_member(){
    $data = $this->M_admin->edit_member();
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Member telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit member!');
    }
    redirect("Admin/Member");
  }

  function delete_member($id_member){
    $data = $this->M_admin->delete_member($id_member);
    if ($data) {
      $this->session->set_flashdata('success', 'Member telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus user!');
    }
    redirect("Admin/Member");
  }

}
