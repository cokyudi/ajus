<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $data['paket'] = $this->M_admin->get_all_paket('tidak promo')->result();
    
    $data['upacara'] = $this->M_admin->get_all_upacara()->result();

    $this->load->view('admin/head');
    $this->load->view('admin/paket',$data);
  }

  function ajax_get_paket(){
    $id_paket = $this->input->post('id_paket');
    $data = $this->M_admin->ajax_get_paket($id_paket);
    echo json_encode($data[0]);
  }

  function ajax_gambar_item($id_item){
    $data['row'] = $this->M_admin->ajax_get_item($id_item);
    $this->load->view('admin/foto_item',$data);
  }

  function add_paket(){
    $tmp = explode(".", $_FILES['foto_paket']['name']);
    $ext = end($tmp);
    $gambar=url_title($this->input->post('nama_paket')).'.'.$ext;
    $config['upload_path']   = './assets/images/paket/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = 10000000;
    $config['max_width']     = 2000;
    $config['max_height']    = 2000;
    $config['file_name']   = $gambar;
    $config['overwrite'] = TRUE;
  
    $this->upload->initialize($config);
    $upload=$this->upload->do_upload('foto_paket');

    $add = [
      'nama_paket'=>$this->input->post('nama_paket'),
      'upacara_id'=>$this->input->post('upacara_id'),
      'harga_paket'=>$this->input->post('harga_paket'),
      'deskripsi'=>$this->input->post('deskripsi'),
      // 'lama_buat_paket'=>$this->input->post('lama_buat_paket'),
      'foto_paket'=>$gambar
    ];
    $data = $this->M_admin->add_paket($add);

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Paket baru telah ditambahkan!');
    }
    redirect("Admin/Paket");
  }

  function update_paket(){
    $id_paket = $this->input->post('edit_id_paket');
    $tmp = explode(".", $_FILES['edit_foto_paket']['name']);
    $ext = end($tmp);
    $gambar=url_title($this->input->post('edit_nama_paket')).'.'.$ext;
    //unlink("./assets/images/paket/".$gambar);
    $config['upload_path']   = './assets/images/paket/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = 10000000;
    $config['max_width']     = 2000;
    $config['max_height']    = 2000;
    $config['file_name']   = $gambar;
    $config['overwrite'] = TRUE;

    
    $this->upload->initialize($config);
    $upload=$this->upload->do_upload('edit_foto_paket');

    $update = [
      'nama_paket'=>$this->input->post('edit_nama_paket'),
      'upacara_id'=>$this->input->post('edit_nama_upacara'),
      'harga_paket'=>$this->input->post('edit_harga_paket'),
      'deskripsi'=>$this->input->post('edit_deskripsi'),
      // 'lama_buat_paket'=>$this->input->post('edit_lama_buat_paket'),
      'foto_paket'=>$gambar
    ];

    $data = $this->M_admin->edit_paket($update,$id_paket);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'paket telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit paket!');
    }
    redirect("Admin/Paket");
  }

  
  function delete_paket($id_paket){
    $data = $this->M_admin->delete_paket($id_paket);
    if ($data) {
      $this->session->set_flashdata('success', 'paket telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus paket!');
    }
    redirect("Admin/Paket");
  }

  function detail_paket($id_paket){
    $data['row2'] = $this->M_admin->get_nama_paket($id_paket);
    $data['row'] = $this->M_admin->get_item_paket($id_paket);
     $data['item'] = $this->M_admin->get_all_item('all')->result();
    $this->load->view('admin/head');
    $this->load->view('admin/detail_paket',$data);


  }
  function add_item_paket(){
    $id_paket = $this->input->post('id_paket');

    $add = [
      'paket_id'=>$this->input->post('id_paket'),
      'item_id'=>$this->input->post('item_id'),
      'qty'=>$this->input->post('qty')
    ];
    $data = $this->M_admin->add_item_paket($add);

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'item Paket  telah ditambahkan!');
    }
     else {
      $this->session->set_flashdata('error', 'Gagal menambah item paket!');
    }
   redirect("Admin/Paket/detail_paket/".$id_paket);
  }

  function delete_item_paket($id_paket,$id_paket_item){
    $data = $this->M_admin->delete_item_paket($id_paket_item);
    if ($data) {
      $this->session->set_flashdata('success', 'item paket telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus item paket!');
    }
    redirect("Admin/Paket/detail_paket/".$id_paket);
  }

}
