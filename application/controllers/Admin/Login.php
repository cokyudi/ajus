<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
      $this->load->model(array('pesan','m_admin'));
      $this->load->library(array('form_validation'));
  }

  function index()
  {
    if($this->session->userdata('admin_username')){
          redirect('admin/dashboard');
    }
    $this->load->view('admin/login');
  }

  public function prosesLogin(){
    $username=$this->input->post('username');
    $password=$this->input->post('password');

    $cek=$this->m_admin->cek_login($username,MD5($password));

    if ($cek=='false') {
      $this->session->set_flashdata('adminLogin', $this->pesan->danger('Username Atau Password Salah...'));
      redirect('admin/login');
    }else if($cek=='block'){
      $this->session->set_flashdata('adminLogin',$this->pesan->danger('Akun Anda Sudah Di Nonaktifkan Karana Alasan Tertentu...'));
      redirect('admin/login');
    }else{

      $this->session->set_userdata('admin_username',$username);
      $this->session->set_userdata('id',$cek[0]->id_user); 
      $this->session->set_userdata('admin_name',$cek[0]->nama);
      $this->session->set_userdata('admin_role',$cek[0]->id_role);
      $this->session->set_userdata('jenis_item',$cek[0]->id_jenis_item);
      $this->session->set_userdata('foto',$cek[0]->foto);
      redirect('admin/dashboard');
    }
  }

  public function logout(){
    $this->session->unset_userdata('admin_username');
    redirect('admin/login');
  }

}
