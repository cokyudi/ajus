<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $data['konfirmasi'] = $this->M_admin->get_all_konfirmasi()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/konfirmasi',$data);
  }

  function get_detail_pembayaran(){
     $id_transaksi = $this->input->post('id_transaksi');
     $id_transaksi_paket = $this->input->post('id_transaksi_paket');

    $data = $this->M_admin->getItemById($id_transaksi)->result();
    echo json_encode($data);
    // $data_paket = $this->M_admin->getPaketById($id_transaksi_paket)->result();
    // echo json_encode($data_paket);

  }
   function get_detail_pembayaran2(){
     $id_transaksi = $this->input->post('id_transaksi');
     $id_transaksi_paket = $this->input->post('id_transaksi_paket');

    // $data = $this->M_admin->getItemById($id_transaksi)->result();
    // echo json_encode($data);
    $data_paket = $this->M_admin->getPaketById($id_transaksi_paket)->result();
    echo json_encode($data_paket);

  }

  function terima($id_pembayaran){
    $data=array(
        'status_pembayaran' => '2',
      );
      $this->db->where('id_pembayaran', $id_pembayaran)->update('tb_pembayaran',$data);
       redirect("Admin/Konfirmasi");
  }

  function tolak($id_pembayaran){
    $data=array(
        'status_pembayaran' => '0',
      );
      $this->db->where('id_pembayaran', $id_pembayaran)->update('tb_pembayaran',$data);
       redirect("Admin/Konfirmasi");
  }

}