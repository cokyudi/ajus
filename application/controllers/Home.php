<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_member','pesan'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
  }

  function index()
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    $data['items'] = $this->M_member->getAllPaket()->result();
    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['aktif'] = 'paket';
    $this->load->view('header', $data);
    $this->load->view('home',$data);
    $this->load->view('footer');
  }

  function search()
  {
    $search = $this->input->get('search');
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    // $data['items'] = $this->M_member->getPaketByUpacara($id_upacara)->result();
    $data['items'] = $this->M_member->search($search)->result();
    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['aktif'] = 'home';
      // echo $search;

    $data['paket_item'] = $this->M_member->searchItem($search)->result();

    // header('Content-type: application/json');
    // echo json_encode($data['paket_item']);

    $this->load->view('header', $data);
    $this->load->view('search',$data);
    $this->load->view('footer');
  }

  function register(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $nama_member = $this->input->post('nama_member');
    $telp = $this->input->post('telp');
    $alamat = $this->input->post('alamat');
    $kota = $this->input->post('kota');
    $kode_pos = $this->input->post('kode_pos');

    if($email=='' OR $password=='' OR $nama_member=='' OR $telp=='' OR $alamat==''){
      $this->session->set_flashdata('memberRegister', $this->pesan->danger('Terdapat Field yang kosong'));
      redirect('/');
    }

    $cek = $this->M_member->cekEmail($email)->num_rows();

    if($cek>0){
      $this->session->set_flashdata('memberRegister', $this->pesan->danger('Email sudah terdaftar'));
      redirect('/');
    }
    else{
      $data= [
          'email'=>$email,
          'password'=>MD5($password),
          'nama_member'=>$nama_member,
          'telp'=>$telp,
          'alamat'=>$alamat,
          'kota'=>$kota,
          'kode_pos'=>$kode_pos
      ];
      $register = $this->M_member->registerMember($data);

      if($register){
        $this->session->set_flashdata('memberLogin', $this->pesan->sukses('Registrasi Berhasil, Silahkan Login'));
        redirect('/');
      }
      else{
        $this->session->set_flashdata('memberRegister', $this->pesan->danger('Registrasi Gagal'));
        redirect('/');
      }
    }
  }

  function login(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    if($email=='' OR $password==''){
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Email atau Password Tidak Boleh Kosong'));
      redirect('/');
    }

    $cek = $this->M_member->cekMember($email,MD5($password))->row();

    if($cek){
      $chart = $this->M_member->cekChart($cek->id_member)->result();
      $chartPaket = $this->M_member->cekPaketChart($cek->id_member)->result();
      if(count($chart)>0 || count($chartPaket)>0){
        $this->session->set_userdata('chart',count($chart)+count($chartPaket));
      }
      else{
        $this->session->set_userdata('chart','0');
      }
      $this->session->set_userdata('id_member',$cek->id_member);
      $this->session->set_userdata('nama_member',$cek->nama_member);
      $this->session->set_userdata('email',$cek->email);
      $this->session->set_userdata('kota',$cek->kota);
      $this->session->set_userdata('telp',$cek->telp);
      $this->session->set_userdata('kode_pos',$cek->kode_pos);
      $this->session->set_userdata('alamat',$cek->alamat);
      redirect('/');
    }
    else{
      $this->session->set_flashdata('memberLogin', $this->pesan->danger('Email atau Password Salah'));
      redirect('/');
    }
  }

  function logout(){
    $this->session->unset_userdata('id_member');
    $this->session->unset_userdata('nama_member');
    redirect('/');
  }

  function kategori($id_jenis_item){
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    $data['items'] = $this->M_member->getItem($id_jenis_item)->result();
    $data['aktif'] = $id_jenis_item;
    $this->load->view('header',$data);
    $this->load->view('home',$data);
    $this->load->view('footer');
  }

  public function promo()
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }

    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    $data['items'] = $this->M_member->getAllPromo()->result();
    $data['aktif'] = 'promo';
    // var_dump($data['items']);
    $this->load->view('header', $data);
    $this->load->view('promo',$data);
    $this->load->view('footer');
  }
  public function promoitem()
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }

    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    $data['items'] = $this->M_member->getAllPromoitem()->result();
    $data['aktif'] = 'promoitem';
    // var_dump($data['items']);
    $this->load->view('header', $data);
    $this->load->view('promo',$data);
    $this->load->view('footer');
  }

  function upacara($id_upacara)
  {
    $id_member = $this->session->userdata('id_member');
    if ($id_member) {
      $data['notif_payment'] = $this->M_member->getPembayaran($id_member, true)->num_rows();
    } else {
      $data['notif_payment'] = 0;
    }
    $data['jenisItems'] = $this->M_member->getAllJenisItem()->result();
    $data['items'] = $this->M_member->getPaketByUpacara($id_upacara)->result();
    $data['upacara'] = $this->M_member->getAllUpacara()->result();
    $data['aktif'] = 'paket';

    $data['paket_item'] = $this->M_member->getPaketItem($id_upacara)->result();

    // header('Content-type: application/json');
    // echo json_encode($data['paket_item']);
    $this->load->view('header', $data);
    $this->load->view('upacara',$data);
    $this->load->view('footer');
  }

}
