<main class="mt-5 pt-4">
    <div class="container wow fadeIn">

      <!-- Heading -->
      <h2 class="my-5 h2 text-center">Profil</h2>
      <div class="card" style="max-width: 500px; margin: 0 auto; padding: 24px;">
        <form action="<?=site_url('Profil/update')?>" method="post">
          <?php if($this->session->flashdata('memberRegister')){ ?>
                <?=$this->session->flashdata('memberRegister')?>
          <?php } ?>
          <!-- Material input name -->
          
        <input type="hidden" name="email" class="form-control form-control-sm" value="<?= $user['email'] ?>" required>

          <!-- Material input subject -->
          <div class="md-form form-sm">
              <input type="text" name="nama_member" class="form-control form-control-sm" value="<?= $user['nama_member'] ?>" required>
              <label>Nama</label>
          </div>

          <div class="md-form form-sm">
              <input type="text" name="kota" class="form-control form-control-sm" value="<?= $user['kota'] ?>" required>
              <label>Kota</label>
          </div>

          <div class="md-form form-sm">
            <input type="number" name="telp" class="form-control form-control-sm" value="<?= $user['telp'] ?>" required>
            <label>Telepon</label>
          </div>
          
          <div class="md-form form-sm">
              <input type="number" name="kode_pos" class="form-control form-control-sm" value="<?= $user['kode_pos'] ?>" required>
              <label>Kode Pos</label>
          </div>

          <!-- Material textarea message -->
          <div class="md-form form-sm">
              <textarea type="text" name="alamat" class="md-textarea form-control" required><?= $user['alamat'] ?></textarea>
              <label>Alamat</label>
          </div>

          <div class="text-center mt-4 mb-2">
              <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
  </main>
</div>
