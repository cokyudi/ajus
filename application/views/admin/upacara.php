
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Upacara
        <button class="pull-right btn btn-success" type="button" name="btn_add_upacara" id='btn_add_upacara'><i class="fa fa-plus"></i>   Tambah Upacara</button>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List Upacara</h3>
        </div>
        <div class="box-body">
          <table class="table table-striped table-bordered table-hover" id='tb_list_jenis'>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Upacara</th>
                <th width="30%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($upacara as $key) {?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->nama_upacara ?></td>
                  <td class="btn-group">
                    <a href="javascript:void(0)" title="Edit upacara" class="btn btn-warning" onclick="edit_upacara('<?php echo $key->id_upacara ?>')"><i class="fa fa-pencil"></i>  Edit</a>
                    <a href="<?php echo base_url("Admin/Upacara/delete_upacara/".$key->id_upacara."")?>" title="Hapus upacara" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>   Hapus</a>
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>

    <div class="modal fade" id="modal_add_upacara" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-plus"></i>    Tambah Upacara</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Upacara/add_upacara') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Upacara</label>
                <input type="text" class="form-control" name="nama_upacara" placeholder="Nama upacara" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Submit</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_edit_upacara" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-pencil"></i>    Edit Upacara</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Upacara/update_upacara') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="nama">Jenis Item</label>
                <input type="text" class="form-control" name="nama_upacara" placeholder="nama upacara" id='edit_nama_upacara' required>
                <input type="hidden" class="form-control" name="id_upacara" id='edit_id_upacara' required readonly>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   submit</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_kelola_upacara').addClass('active');
    $('#tb_list_jenis').DataTable();
    $('#tittle').text('SIM | Kelola Upacara');
  });

  $('#btn_add_upacara').click(function() {
    $('#modal_add_upacara').modal('show');
  });

  function edit_upacara(id_upacara){
    $.ajax({
      url: '<?php echo base_url('Admin/Upacara/ajax_get_upacara') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
        id_upacara: id_upacara,
      },
      success:function(data){
        console.log(data);
        $('#edit_id_upacara').val(data.id_upacara);
        $('#edit_nama_upacara').val(data.nama_upacara);
      },
      error:function(){
        alert('error get anggota');
      }
    });
    $('#modal_edit_upacara').modal('show');
  }
</script>
</body>
</html>
