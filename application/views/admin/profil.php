
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Profil  <b><?php echo $this->session->userdata('admin_name') ?></b>

      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
         
        </div>
        <div class="box-body">
          <?php foreach ($profil as $key) { ?>
          <form  class="" action="<?php echo base_url('Admin/User/update_profil') ?>" method="post" enctype="multipart/form-data">
                  <input    type='text' name="id_supp" value="<?php echo $key->id_user ?>" hidden /> 


                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Username :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control'  type='text'  value="<?php echo $key->username ?>" readonly /> 
                        </div>
                  </div>
                    <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Nama :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input class='form-control' name='nama' type='text'  value="<?php echo $key->nama ?>"  /> 
                        </div>
                  </div>
                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Jenis Supplier :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control' name='jenis_supplier' type='text'  value="<?php echo $key->jenis_item ?>" readonly /> 
                        </div>
                  </div>
                   <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">No Hp :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control' name='no_hp' type='text'  value="<?php echo $key->no_hp ?>"  /> 
                        </div>
                  </div>
                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Email :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control' name='email' type='text'  value="<?php echo $key->email ?>"  /> 
                        </div>
                  </div>
                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Kota :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control' name='kota' type='text'  value="<?php echo $key->kota ?>"  /> 
                        </div>
                  </div>

                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Alamat :</label>
                         <div class="col-sm-7 control-label" style="text-align:left;">
                           <textarea   class='form-control' name='alamat'  ><?php echo $key->alamat ?></textarea>
                         </div>
                  </div>
                  <div class="col-xs-12 form-group">
                        <label class="col-sm-3 control-label" style="text-align:right;">Foto :</label>
                        <div class="col-sm-7 control-label" style="text-align:left;">
                          <input  class='form-control' name='foto' type='file'    /> 
                        </div>
                  </div>
                   <div class="col-xs-12 form-group" >
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-7 control-label">
                      <div class="btn-group pull-right">
                          <button type="submit"  name="add" value="Simpan" class="btn btn-success btn-flat">Edit</button>
  
                    
                    </div>
                  </div>
                </div>
          </form>
          <?php } ?>
        </div>
      </div>
    </section>

   

    

    <div class="modal fade" id="modal_edit_harga" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-pencil"></i>    Edit Harga</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_harga') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="harga_nama_item" name="nama_item" placeholder="Nama Item" readonly required>
                <input type="hidden" class="form-control" id="harga_id_item" name="id_item" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" id="harga_harga_modal" name="harga_modal" placeholder="Harga Modal" readonly required>
              </div>
              <div class="form-group">
                <label for="username">Harga Jual</label>
                <input type="text" min="" class="form-control" id="harga_harga_jual" name="harga_jual" placeholder="Harga Jual" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | Profil');
  });

  $('#btn_add_item').click(function() {
    $('#modal_add_item').modal('show');
  });

  function edit_item_admin(id_item){
    $.ajax({
      url: '<?php echo base_url('Admin/Item/ajax_get_item') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_item: id_item,
      },
      success:function(data){
        console.log(data);
        $('#harga_id_item').val(data.id_item);
        $('#harga_nama_item').val(data.nama_item);
        $('#harga_harga_modal').val(data.harga_modal);
        $('#jenis_transaksi').val(data.jenis_transaksi);
        $('#harga_harga_jual').val(data.harga_jual);
        $('#harga_harga_jual').attr('min', data.harga_modal);
        $('#modal_edit_harga').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

   function edit_item(id_item){
    $.ajax({
      url: '<?php echo base_url('Admin/Item/ajax_get_item') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_item: id_item,
      },
      success:function(data){
        console.log(data);
        $('#id_item').val(data.id_item);
        $('#nama_item').val(data.nama_item);
        $('#lama_buat').val(data.lama_buat);
        $('#jenis_transaksi').val(data.jenis_transaksi);
        $('#jenis_transaksi').html(data.jenis_transaksi);
        $('#lama_tahan').val(data.lama_tahan);
        $('#harga_modal').val(data.harga_modal);
        $('#modal_edit_item').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

 

  

</script>
</body>
</html>


