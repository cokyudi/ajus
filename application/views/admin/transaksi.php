
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->



    <!-- Main content -->

       <section class="content-header">
      <?php
       $tab1='active';
       $tab2='';
       $tab11='active';
       $tab22='';
      if ($this->session->flashdata('item')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('item')?></p>
        </div>
      <?php
       $tab1='';
       $tab2='active';
        $tab11='';
       $tab22='active';
      endif;

      if ($this->session->flashdata('paket')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('paket')?></p>
        </div>
      <?php
      endif;?>
      <h1>
        Kelola Transaksi

      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?php echo $tab11 ?>"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> Transaksi Barang</a></li>
              <li class="<?php echo $tab22 ?>"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Transaksi Paket</a></li>
            </ul>
            <div class="tab-content">

              <div class="tab-pane <?php echo $tab1 ?>" id="tab_1">

                 <div id="tabel_paket" class="box box-solid box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Transaksi Barang</h3>
                    </div>
                    <div class="box-body">
                      <table class="table table-striped table-bordered table-hover" id='tb_list_transaksi'>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Member</th>
                            <th>Tanggal Transaksi</th>
                            <th>Total Harga</th>
                            <th>Status Transaksi</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          <?php foreach ($transaksi as $key) {?>
                            <tr>
                              <td><?php echo $no ?></td>
                              <td><?php echo $key->nama_member ?></td>
                              <td><?php echo $key->tgl_transaksi ?></td>
                              <td>Rp. <?php echo number_format($key->total_harga,0,".",".")  ?></td>
                              <td>
                                 <?php if($key->status_transaksi==0){ ?>
                                      Belum konfirmasi
                                <?php }if($key->status_transaksi==1){ ?>
                                      Sudah Upload Pembayaran
                                <?php }
                                if($key->status_transaksi==2){ ?>
                                      Sudah dikonfirmasi
                                <?php } ?>
                              </td>

                              <td class="btn-group">
                                <a 
                                  href="javascript:void(0)" 
                                  title="Detail" 
                                  class="btn btn-info" 
                                  onclick="detail_item(
                                    '<?php echo $key->id_transaksi ?>',
                                    '<?= $key->kota ?>',
                                    '<?= $key->kode_pos ?>',
                                    '<?= $key->telp ?>',
                                    '<?= $key->alamat ?>'
                                  )">
                                    <i class="fa fa-eye"></i>  Detail
                                </a>
                              </td>
                            </tr>
                            <?php $no++; ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane <?php echo $tab2 ?>" id="tab_2">


                 <div id="tabel_paket" class="box box-solid box-success">
                    <div class="box-header with-border">
                      <h3 class="box-title">Transaksi Paket</h3>
                    </div>
                    <div class="box-body">
                     <table class="table table-striped table-bordered table-hover" id='tb_list_transaksi2'>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Member</th>
                            <th>Tanggal Transaksi</th>
                            <!-- <th>Total Harga</th> -->
                            <th>Status Transaksi</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          <?php foreach ($transaksiPaket as $key) {?>
                            <tr>
                              <td><?php echo $no ?></td>
                              <td><?php echo $key->nama_member ?></td>
                              <td><?php echo $key->tgl_transaksi ?></td>
                              <!-- <td><?php echo $key->total_harga ?></td> -->
                              <td>
                                <?php if($key->status_transaksi==0){ ?>
                                      Belum konfirmasi
                                <?php }if($key->status_transaksi==1){ ?>
                                      Sudah Upload Pembayaran
                                <?php }
                                if($key->status_transaksi==2){ ?>
                                      Sudah dikonfirmasi
                                <?php } ?>

                                </td>

                              <td class="btn-group">
                                <a 
                                  href="javascript:void(0)" 
                                  title="Detail" 
                                  class="btn btn-success" 
                                  onclick="detail(
                                    '<?php echo $key->id_transaksi_paket ?>',
                                    '<?= $key->kota ?>',
                                    '<?= $key->kode_pos ?>',
                                    '<?= $key->telp ?>',
                                    '<?= $key->alamat ?>'
                                  )">
                                    <i class="fa fa-eye"></i>  Detail
                                </a>
                              </td>
                            </tr>
                            <?php $no++; ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>

              </div>
            </div>
            <!-- /.tab-content -->
          </div>


    </section>




   <div class="modal fade" id="modal_detail_paket" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Detail Transaksi</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class=""  method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="edit_id_paket" id="edit_id_paket" >
              <div class="form-group col-xs-9">
                <label for="username">Nama Paket</label>
                <input type="text" class="form-control" id="nama_paket"  readonly>

              </div>
              <div class="form-group col-xs-3">
                <label style="color: white" for="username">n</label>
                <a class="btn btn-warning btn-flat form-control" type="submit" name="button" onclick="showdetail()">Detail Paket</a>
              </div>
              <br>
               <div id="detail_paket" class="form-group col-xs-12">
                <label for="username">Detail Paket</label><br>
                <ol ></ol>

              </div>

              <div class="form-group col-xs-12">
                <label for="username">Harga Paket</label>
                <input type="text" class="form-control" id="harga_paket" readonly>
              </div>
              
              <div class="form-group col-xs-12">
                <label for="username">Tanggal Upacara</label>
                <input type="text" class="form-control" id="tgl_upacara" readonly>
              </div>
              <!--  <div class="form-group col-xs-12">
                <label for="username">Deskiripsi Pesanan</label>
                <textarea type="text" class="form-control" id="pesan_detail" readonly></textarea>
              </div> -->
              <div class="form-group col-xs-12">
                <label for="username">Kota</label>
                <input type="text" class="form-control" id="kota" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Nomor Telepon</label>
                <input type="text" class="form-control" id="telp" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Alamat</label>
                <textarea type="text" class="form-control" id="alamat" readonly></textarea>
              </div>
              <div style="color: white;" class="form-group">
                a
              </div>



              <div class="form-group col-xs-12 ">
               <a href="" class="btn btn-success btn-flat pull-right" id="lihat-jadwal">Lihat Jadwal</a>
               <button type="button" class="btn btn-danger btn-flat pull-right" data-dismiss="modal">tutup</button>
              </div>
              <br>
              <br>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

      <div class="modal fade" id="modal_detail_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Detail Transaksi</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class=""  method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="edit_id_paket" id="edit_id_paket" >
              <br>

              <div class="form-group col-xs-12">
                <label for="username">Harga Total</label>
                <input type="text" class="form-control" id="total_harga" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Tanggal Transaksi</label>
                <input type="text" class="form-control" id="tgl_transaksi" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Tanggal Upacara</label>
                <input type="text" class="form-control" id="tgl_upacara_item" readonly>
              </div>
               <div id="detail_paket" class="form-group col-xs-12">
                <label for="username">Detail Item</label><br>
                <ol id="item"></ol>

              </div>

              <div class="form-group col-xs-12">
                <label for="username">Kota</label>
                <input type="text" class="form-control" id="kota_item" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos_item" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Nomor Telepon</label>
                <input type="text" class="form-control" id="telp_item" readonly>
              </div>
              <div class="form-group col-xs-12">
                <label for="username">Alamat</label>
                <textarea type="text" class="form-control" id="alamat_item" readonly></textarea>
              </div>

              <div style="color: white;" class="form-group">
                a
              </div>




              <div class="form-group col-xs-12 ">

               <button type="button" class="btn btn-danger btn-flat pull-right" data-dismiss="modal">tutup</button>
              </div>
              <br>
              <br>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_transaksi').addClass('active');
    $('#tb_list_transaksi').DataTable();
     $('#tb_list_transaksi2').DataTable();
    $('#tittle').text('SIM | Transaksi');
  });

  $('#btn_add_item').click(function() {
    $('#modal_add_item').modal('show');
  });

 function detail(id_transaksi_paket, kota, kode_pos, telp, alamat){
   $("ol").empty();
   $('#detail_paket').css('display','none');
    $.ajax({
      url: '<?php echo base_url('Admin/Transaksi/ajax_get_detail') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi_paket: id_transaksi_paket,
      },
      success:function(data){
          


          //format rupiah
          var bilangan = data.harga;
            
          var number_string = bilangan.toString(),
              sisa    = number_string.length % 3,
              rupiah  = number_string.substr(0, sisa),
              ribuan  = number_string.substr(sisa).match(/\d{3}/g);
              
          if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
        console.log(data);
        $('#nama_paket').val(data.nama_paket);
        $('#harga_paket').val('Rp. '+rupiah);
        // $('#qty_item').val(data.qty_item);
        $('#pesan_detail').val(data.pesan_detail);
        $('#tgl_upacara').val(data.tgl_upacara);
        $('#kota').val(kota);
        $('#kode_pos').val(kode_pos);
        $('#telp').val(telp);
        $('#alamat').val(alamat);
        $('#lihat-jadwal').attr('href', '<?= site_url() ?>Admin/Transaksi/detail/'+id_transaksi_paket)

        $.ajax({
          url: '<?php echo base_url('Admin/Transaksi/ajax_get_detail_paket') ?>',
          type: 'POST',
          dataType: 'JSON',
          data: {
          id_paket: data.id_item,
          },
          success:function(data2){
            console.log(data2);
             for (i = 0; i < data2.length; i++) {
              $("ol").append("<li>"+data2[i].nama_item+"</li>");
              }
          },
          error:function(){
            alert('error get anggota');
          }
        });
        $('#modal_detail_paket').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

  function detail_item(id_transaksi, kota, kode_pos, telp, alamat){
   $("ol").empty();
   //$('#detail_paket').css('display','none');
    $.ajax({
      url: '<?php echo base_url('Admin/Transaksi/ajax_get_detailItem') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi: id_transaksi,
      },
      success:function(data){


          //format rupiah
          var bilangan = data.total_harga;
            
          var number_string = bilangan.toString(),
              sisa    = number_string.length % 3,
              rupiah  = number_string.substr(0, sisa),
              ribuan  = number_string.substr(sisa).match(/\d{3}/g);
              
          if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }

        console.log(data);
        $('#total_harga').val('Rp. '+rupiah);
        $('#tgl_transaksi').val(data.tgl_transaksi);
        $('#kota_item').val(kota);
        $('#kode_pos_item').val(kode_pos);
        $('#telp_item').val(telp);
        $('#alamat_item').val(alamat);

         $.ajax({
            url: '<?php echo base_url('Admin/Konfirmasi/get_detail_pembayaran') ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
            id_transaksi: id_transaksi,
            },
            success:function(data){


              for (i = 0; i < data.length; i++) {
                   $("#item").append("<li>"+data[i].nama_item+"  <b>( "+data[i].qty_item+" buah )</b></li>");
                    $('#tgl_upacara_item').val(data[i].tgl_upacara);
                }
                 if (data.length==0) {

              }

            },
            error:function(){
              alert(id_transaksi);
            }
          });
        $('#modal_detail_item').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

  function showdetail() {
    $('#detail_paket').css('display','block');
  }



</script>
</body>
</html>
