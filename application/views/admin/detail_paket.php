 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Paket
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div id="tabel_paket" class="box box-solid box-warning">
        <div class="box-header with-border">
        	<?php foreach ($row2 as $key2) { ?>
          <h3 class="box-title">Detail Item : <td><?php echo $key2->nama_paket ?></td></h3>
          
           
        </div>
        <div class="box-body">
        	 <a href="javascript:void(0)" title="tambah item" class="btn btn-success pull-right" onclick="tambah_item_paket('<?php echo $key2->id_paket ?>')"><i class="fa fa-plus"></i>  Tambah Item </a>
        <?php } ?> 
          <table class="table table1 table-striped table-bordered table-hover" >
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Item</th>
                <th>Deskripsi Item</th>
                <th>Jumlah</th>
                <th style="text-align: center;width: 15%;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($row as $key) {?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->nama_item ?></td>
                  <td><?php echo $key->deskripsi ?></td>
                  <td><?php echo $key->qty ?></td>
                  <td  class="btn-group">
                    <a href="<?php echo base_url("Admin/Paket/delete_item_paket/".$key->paket_id."/".$key->id_paket_item)?>" title="Hapus member" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>   Hapus</a>
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
         </div>

    </section>

   
    

     <div class="modal fade" id="modal_tambah_item_paket" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Tambah Item Paket</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Paket/add_item_paket') ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="id_paket" id="id_paket" >
              <div class="form-group">
                <label for="username">Item</label>
                 <select name="item_id"  class="form-control"  >
                   <?php foreach ($item as $data) {
                      echo ' <option value="'.$data->id_item.'"> '.$data->nama_item.' </option>  ';
                   }
                   ?>

                                          
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Jumlah</label>
                <input type="text" class="form-control" name="qty" placeholder="Jumlah item" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>

          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

   

    

  </div>
  <!-- /.content-wrapper -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_kelola_paket').addClass('active');
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | Kelola Paket');
  });


   function tambah_item_paket(id_paket){
     $('#id_paket').val(id_paket);
        $('#modal_tambah_item_paket').modal('show');
  }

  function detail_paket(id_paket)
      {
        $("#tabel_paket").css('display','none'); 
        $("#btn_add_paket").css('display','none'); 
        
       $.ajax({
         url:"<?php echo base_url();?>Admin/Paket/detail_paket/"+id_paket+"",
         success: function(response){
         $("#detail_paket").html(response);
         },
         dataType:"html"
       });

       return false;
  }

</script>
</body>
</html>


