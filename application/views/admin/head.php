<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title id="tittle"></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css') ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/_all-skins.min.css') ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/morris.js/morris.css') ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/jvectormap/jquery-jvectormap.css') ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>">
  <link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SIM</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SIM Event Organizer</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
        <?php if($this->session->userdata('admin_role')=='supplier'){ ?>  
             <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning"><?php echo $jum_notif ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"><?php echo $jum_notif ?> Pemberitahuan</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                       <?php foreach ($notif as $key) {?>
              
                          <li>
                            <a href="<?php echo base_url('Admin/Item/detail_notif/'.$key->id_notif.'/'.$key->item_id.'/'.$key->status_item.'') ?>">
                              <i class="fa fa-bullhorn "></i> <?php echo implode(" ", array_slice(explode(" ", $key->nama_item ), 0, 2)); ?> <b> <?php echo $key->status_item ?> </b> <br>
                               <i> <?php echo $key->tanggal ?> </i> <br>
                               <b>Lihat detail</b>
                            </a>
                          </li>

                        <?php } ?>  
                    </ul>
                  </li>
                </ul>
              </li>
        <?php } ?>

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/images/profil/'.$this->session->userdata('foto').'') ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('admin_name') ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/images/profil/'.$this->session->userdata('foto').'') ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('nama') ?>
                  <small><?php echo $this->session->userdata('admin_role') ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <?php if($this->session->userdata('admin_role')=='supplier'){ ?>
                <div class="pull-left">
                  <a href="<?php echo base_url('Admin/User/profil') ?>" class="btn btn-default btn-block btn-flat ">Profil</a>
                </div>
              <?php } ?>
                <div class="pull-right">
                  <a href="<?php echo base_url('Admin/Login/logout') ?>" class="btn btn-default btn-block btn-flat ">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/images/profil/'.$this->session->userdata('foto').'') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('admin_name') ?></p>
           <small><?php echo $this->session->userdata('admin_role') ?></small>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" style="font-size: 12.5px">
        <li class="header">MAIN MENU</li>
<?php if($this->session->userdata('admin_role')=='admin'){ ?>
        <li class="" id='btn_dashboard'>
          <a href="<?php echo base_url('Admin/Dashboard') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="" id='btn_kelola_user'>
          <a href="<?php echo base_url('Admin/User/') ?>">
            <i class="fa fa-user"></i> <span>Kelola User</span>
          </a>
        </li>
        <li class="" id='btn_kelola_supplier'>
          <a href="<?php echo base_url('Admin/Supplier/') ?>">
            <i class="fa fa-user"></i> <span>Kelola Supplier</span>
          </a>
        </li>
        <li class="" id='btn_kelola_member'>
          <a href="<?php echo base_url('Admin/Member/') ?>">
            <i class="fa fa-user"></i> <span>Kelola Member</span>
          </a>
        </li>
        <li class="" id='btn_kelola_upacara'>
          <a href="<?php echo base_url('Admin/Upacara/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Kelola Upacara</span>
          </a>
        </li>
        <li class="" id='btn_kelola_paket'>
          <a href="<?php echo base_url('Admin/Paket/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Kelola Paket</span>
          </a>
        </li>
        <li class="" id='btn_kelola_jenis_item'>
          <a href="<?php echo base_url('Admin/JenisItem/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Kelola Jenis Item</span>
          </a>
        </li>
         <li class="" id='btn_konfirmasi_item'>
          <a href="<?php echo base_url('Admin/KonfirmasiItem/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Konfirmasi Item</span>
          </a>
        </li>
         
<?php } ?>

<?php if($this->session->userdata('admin_role')=='supplier'){ ?>
        <li class="" id='btn_pemberitahuan'>
          <a href="<?php echo base_url('Admin/Pemberitahuan') ?>">
            <i class="fa fa-address-book"></i>
            <span>Pemberitahuan</span>
          </a>
        </li>
<?php } ?>
        <li class="" id='btn_kelola_item'>
          <a href="<?php echo base_url('Admin/Item/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Kelola Item</span>
          </a>
        </li>
<?php if($this->session->userdata('admin_role')=='admin'){ ?>
        <li class="" id='btn_kelola_promo'>
          <a href="<?php echo base_url('Admin/Promo/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Kelola Promo</span>
          </a>
        </li>
        <li class="" id='btn_transaksi'>
          <a href="<?php echo base_url('Admin/Transaksi/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Transaksi</span>
          </a>
        </li>

        <li class="" id='btn_konfirmasi'>
          <a href="<?php echo base_url('Admin/Konfirmasi/') ?>">
            <i class="fa fa-address-book"></i>
            <span>Konfirmasi</span>
          </a>
        </li>
<?php } ?>
<?php if($this->session->userdata('admin_role')=='supplier'){ ?>
        <li class="" id='btn_pesanan'>
          <a href="<?php echo base_url('Admin/Pemesanan/') ?>">
            <i class="fa fa-address-book"></i>
            <span>History Transaksi</span>
          </a>
        </li>
<?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
