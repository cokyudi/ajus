
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Item
  <?php if($this->session->userdata('admin_role')=='supplier'){ ?>
        <button class="pull-right btn btn-success" type="button" name="btn_add_item" id='btn_add_item'><i class="fa fa-plus"></i>   Tambah Item</button>
  <?php } ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List Item</h3>
        </div>
        <div class="box-body">
          <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item'>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Item</th>
                <th>Jenis Item</th>
                <th>Supplier</th>
                <th>Minimal Boking</th>
                <th>Lama Tahan</th>
                <th>Harga Modal</th>
                <th>Harga Jual</th>
                <th>Foto</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($item as $key) {?>
                <tr>
                  <td width="5%"><?php echo $no ?></td>
                  <td width="15%"><?php echo $key->nama_item ?></td>
                  <td  width="10%"><?php echo $key->jenis_item ?></td>
                  <td  width="10%"><?php echo $key->nama ?></td>
                  <td><?php echo $key->lama_buat ?> hari</td>
                  <td><?php echo $key->lama_tahan ?> Hari</td>
                  <td>Rp. <?php echo number_format($key->harga_modal,0,".",".")  ?></td>
                  <td>Rp. <?php echo number_format($key->harga_jual,0,".",".")  ?></td>
                  <td  width="10%"><?php echo $key->foto_item ?></td>
                  <td class="btn-group">
          <?php if($this->session->userdata('admin_role')=='supplier'){ ?>
                    <a href="javascript:void(0)" title="Edit Item" class="btn btn-warning btn-flat" onclick="edit_item('<?php echo $key->id_item ?>')"><i class="fa fa-pencil"></i>  </a>
          <?php } else { ?>
                    <a href="javascript:void(0)" title="Edit Item" class="btn btn-warning btn-flat" onclick="edit_item_admin('<?php echo $key->id_item ?>')"><i class="fa fa-pencil">  </i>   </a>
          <?php } ?><br>
                    <a href="<?php echo base_url("Admin/Item/delete_item/".$key->id_item."")?>" title="Hapus Item" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-flat btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>   </a>
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>

    <div class="modal fade" id="modal_add_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-plus"></i>    Tambah Item</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/add_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" name="nama_item" placeholder="Nama Item" required>
              </div>
              <div class="form-group">
                <label for="username">Jenis Item</label>
                <input type="text" class="form-control" name="jenis_item" placeholder="Nama Item" value="<?=$jenis_item[0]->jenis_item?>" required readonly="">
              </div>
              <div class="form-group">
                <label for="username">Minimal Boking</label>
                <input type="text" class="form-control" name="lama_buat" placeholder="Lama Buat" required>
              </div>
              <div class="form-group">
                <label for="username">Jenis Transaksi</label>
                <select name="jenis_transaksi"  class="form-control" >
                  <option value="jual"> Jual </option> 
                  <option value="sewa"> Sewa </option>
                </select>
              </div>
              <div class="form-group">
                <label for="username">Lama Tahan</label>
                <input type="text" class="form-control" name="lama_tahan" placeholder="Lama Tahan" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" name="harga_modal" placeholder="Harga Modal" required>
              </div>
              <div class="form-group">
                <label for="username">Deskripsi</label>
                <textarea class="form-control" name="deskripsi" placeholder="Deskripsi" required></textarea>
              </div>
              <div class="form-group">
                <label for="username">Foto Item</label>
                <input type="file" class="form-control" name="foto_item" placeholder="Foto Item" required>
              </div>
              <center><b>Preview Foto</b></center><br>
              <div align="center">
                <img id="preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">
              </div><br>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_edit_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-pencil"></i>    Edit Item</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="nama_item" name="nama_item" placeholder="Nama Item" required>
                <input type="hidden" class="form-control" id="id_item" name="id_item" required>
              </div>
              <div class="form-group">
                <label for="username">Jenis Item</label>
                <input type="text" class="form-control" id="jenis_item" name="jenis_item" placeholder="Nama Item" value="<?=$jenis_item[0]->jenis_item?>" required readonly="">
              </div>
              <div class="form-group">
                <label for="username">Boking</label>
                <input type="text" class="form-control" id="lama_buat" name="lama_buat" placeholder="Lama Buat" required>
              </div>
               <div class="form-group">
                <label for="username">Jenis Transaksi</label>
                <select name="jenis_transaksi"  class="form-control" >
                   <option id="jenis_transaksi" >  </option>
                   <option value="jual"> jual </option> 
                   <option value="sewa"> sewa </option>
                </select>
              </div>
              <div class="form-group">
                <label for="username">Lama Tahan</label>
                <input type="text" class="form-control" id="lama_tahan" name="lama_tahan" placeholder="Lama Tahan" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" id="harga_modal" name="harga_modal" placeholder="Harga Modal" required>
              </div>
              <div class="form-group">
                <label for="username">Deskripsi</label>
                <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi" required></textarea>
              </div>
              <div class="form-group">
                <label for="username">Foto Item</label>
                <input type="file" class="form-control" name="edit_foto_item" placeholder="Foto Item" required>
              </div>
              <center><b>Preview Foto</b></center><br>
              <div align="center">
                <img id="edit_preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">
              </div><br>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_edit_harga" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-pencil"></i>    Edit Harga</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_harga') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="harga_nama_item" name="nama_item" placeholder="Nama Item" readonly required>
                <input type="hidden" class="form-control" id="harga_id_item" name="id_item" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" id="harga_harga_modal" name="harga_modal" placeholder="Harga Modal" readonly required>
              </div>
              <div class="form-group">
                <label for="username">Harga Jual</label>
                <input type="text" min="" class="form-control" id="harga_harga_jual" name="harga_jual" placeholder="Harga Jual" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
 <!--  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer> -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_kelola_item').addClass('active');
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | List Item');
  });

  $('#btn_add_item').click(function() {
    $('#modal_add_item').modal('show');
  });

  function edit_item_admin(id_item){
    $.ajax({
      url: '<?php echo base_url('Admin/Item/ajax_get_item') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_item: id_item,
      },
      success:function(data){
        console.log(data);
        $('#harga_id_item').val(data.id_item);
        $('#harga_nama_item').val(data.nama_item);
        $('#harga_harga_modal').val(data.harga_modal);
        $('#jenis_transaksi').val(data.jenis_transaksi);
        $('#harga_harga_jual').val(data.harga_jual);
        $('#harga_harga_jual').attr('min', data.harga_modal);
        $('#modal_edit_harga').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

   function edit_item(id_item){
    $.ajax({
      url: '<?php echo base_url('Admin/Item/ajax_get_item') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_item: id_item,
      },
      success:function(data){
        console.log(data);
        $('#id_item').val(data.id_item);
        $('#nama_item').val(data.nama_item);
        $('#lama_buat').val(data.lama_buat);
        $('#jenis_transaksi').val(data.jenis_transaksi);
        $('#jenis_transaksi').html(data.jenis_transaksi);
        $('#lama_tahan').val(data.lama_tahan);
        $('#harga_modal').val(data.harga_modal);
        $('#deskripsi').val(data.deskripsi);        
        $('#modal_edit_item').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

  $('input[name=foto_item]').change(function() {
    readURL(this);
  });

  $('input[name=edit_foto_item]').change(function() {
    editReadURL(this);
  });

  function readURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        if(input.name=='foto_item'){
          $('#preview_foto').attr('src', e.target.result);
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
    else{
      if(input.name=='foto_item'){
        var img = '<img id="preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">';
        $('#preview_foto').replaceWith(img);
      }
    }
  }

  function editReadURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        if(input.name=='edit_foto_item'){
          $('#edit_preview_foto').attr('src', e.target.result);
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
    else{
      if(input.name=='edit_foto_item'){
        var img = '<img id="edit_preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">';
        $('#preview_foto').replaceWith(img);
      }
    }
  }
</script>
</body>
</html>


