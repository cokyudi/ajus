<main class="mt-5 pt-4">
    <div class="container wow fadeIn">

      <!-- Heading -->
      <h2 class="my-5 h2 text-center">Pembayaran</h2>

      <!--Grid row-->
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Transaksi Item</th>
              <!-- <th scope="col">Transaksi Paket</th> -->
              <th scope="col">Bank tujuan</th>
              <th scope="col">Pemilik Rekening</th>
              <th scope="col">Nomor Rekening</th>
              <th scope="col">Tanggal Pembayaran</th>
              <th scope="col">Status</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
    <?php foreach ($pembayaran as $key => $value) { ?>
          <tr>
              <th scope="row"><?=$key+1;?></th>
              <td><button type="button" onclick="lihatItem(<?=$value->id_transaksi;?>, <?=$value->id_transaksi_paket;?>)" class="btn btn-sm btn-default">Lihat</button></td>
              <!-- <td><button type="button" onclick="lihatPaket(<?=$value->id_transaksi_paket;?>)" class="btn btn-sm btn-primary">Lihat</button></td> -->
              <td><?=$value->bank_tujuan;?></td>
              <td><?=$value->pemilik_rekening;?></td>
              <td><?=$value->nomor_rekening;?></td>
              <td><?=$value->tgl_pembayaran;?></td>
              <td>
            <?php if($value->status_pembayaran==0){ ?>
                  Belum konfirmasi
            <?php }if($value->status_pembayaran==1){ ?>
                  Upload sukses
            <?php }
            if($value->status_pembayaran==2){ ?>
                  Sudah dikonfirmasi
            <?php } ?>
              </td>
              <td>
                <?php if($value->status_pembayaran==0){ ?>
                  <button type="button" onclick="konfirmasi(<?=$value->id_pembayaran;?>)" class="btn btn-sm btn-warning">Konfirmasi</button>
            <?php }else{ ?>
                  <button type="button" onclick='lihatKonfirmasi(<?php echo json_encode($value); ?>)' class="btn btn-sm btn-warning">Lihat Konfirmasi</button>
            <?php } ?>
              </td>
            </tr>
    <?php } ?>
          </tbody>
        </table>
      </div>

    </div>
  </main>
  <!--Main layout-->
  <div class="modal" id="modal_lihatItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header special-color white-text">
                <h4 class="title">Transaksi Item</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <ol></ol>
            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <div class="modal" id="modalKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header special-color white-text">
                <h4 class="title">
                    <i class="fa fa-pencil"></i> Bukti Pembayaran</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <form action="<?=site_url('Pembayaran/konfirmasi')?>" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="cc-name">Pemilik Rekening</label>
                    <input type="text" class="form-control" name="pemilik_rekening" id="cc-name" placeholder="" required>
                    <div class="invalid-feedback">
                      Name on card is required
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <label for="cc-number">Nomor Rekening</label>
                    <input type="number" class="form-control" name="nomor_rekening" id="cc-number" placeholder="" required>
                    <div class="invalid-feedback">
                      Credit card number is required
                    </div>
                  </div>
                </div>
                <!-- Material input name -->
                <div class="md-form form-sm">
                    <input type="file" name="bukti_pembayaran" class="form-control form-control-sm">
                    <input type="hidden" id="id_pembayaran" name="id_pembayaran">
                </div>

                <div class="text-center mt-4 mb-2">
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
              </form>

            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <div class="modal" id="modalLihatKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header special-color white-text">
                <h4 class="title">
                    <i class="fa fa-pencil"></i> Bukti Pembayaran</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <form action="<?=site_url('Pembayaran/konfirmasi')?>" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="cc-name">Pemilik Rekening</label>
                    <p id="pemilik-rekening"></p>
                  </div>
                  <div class="col-md-6 mb-3">
                    <label for="cc-number">Nomor Rekening</label>
                    <p id="nomor-rekening"></p>
                  </div>
                </div>
                <!-- Material input name -->
                <div id="image" style="text-align:center"></div>
              </form>

            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <script type="text/javascript">

    function lihatItem(id_item, id_paket){
      $.ajax({
        url: "<?php echo site_url('/Keranjang/lihatItem/')?>"+id_item+'/'+id_paket,
        type: "POST",
        dataType: 'JSON',
        cache: false,
        success: function(data){
          $("ol").html('')
          if (data[0].nama_item) {
            for (i = 0; i < data.length; i++) {
              $("ol").append("<li>"+data[i].nama_item+"</li>");
            }
          } else {
            for (i = 0; i < data.length; i++) {
              $("ol").append("<li>"+data[i].nama_paket+"</li>");
            }
          }
          
          $("#modal_lihatItem").modal();
        }
      });
    }

    function lihatKonfirmasi(data) {
      $('#modalLihatKonfirmasi').modal();
      $('#pemilik-rekening').html(data.pemilik_rekening)
      $('#nomor-rekening').html(data.nomor_rekening)
      $('#image').html('<img src="<?= site_url() ?>/assets/images/bukti/'+data.bukti_pembayaran+'" width="300" />')
    }

    function konfirmasi(id){
      $('#id_pembayaran').val(id);
      $('#modalKonfirmasi').modal();
    }

  </script>
