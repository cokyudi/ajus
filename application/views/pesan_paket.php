<!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container dark-grey-text mt-5">

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <img src="<?=base_url("assets/images/paket/$paket->foto_paket")?>" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <!--Content-->
          <div class="p-4">

            <div class="mb-3">
              <h1><span class="badge indigo mr-1"><?=$paket->nama_paket?></span></h1>
            </div>

            <p class="lead">
              <span>Rp. <?=number_format($paket->harga_paket,'0','.','.');?></span>
            </p>

            <p class="lead font-weight-bold">Deskripsi</p>
            <p><?= $paket->deskripsi ?></p>
            <table>
              <?php foreach ($paket_item as $key => $pi): ?>
                <tr>
                  <td width="24" style="font-size:16px !important;"><?= $key+1 ?>.</td>
                  <td style="font-size:16px !important;"><?= $pi->nama_item ?></td>
                  <td style="font-size:16px !important;"><b>(<?= $pi->qty ?>)</b></td>
                </tr>
              <?php endforeach ?>
            </table>

            <form action="<?=site_url('Pesanan/masukChartPaket')?>" method="post">
              <!-- Default input -->
            <div class="row">
              <div class="col-md-12">
                  <label>Tanggal Upacara</label>
                  <div class="input-group date" data-provide="datepicker">
                      <input type="text" name="tgl_upacara" placeholder="Tanggal Upacara" class="form-control" required>
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                  <br>
                  <input type="hidden" value="1" name="jenis_item" class="form-control">
                  <input type="hidden" value="<?=$paket->id_paket?>" name="id_item" class="form-control">
                  <input type="hidden" value="<?=$paket->harga_paket?>" name="harga" class="form-control">
                  <br>
              </div>
              <div class="col-md-12">
                <button class="btn btn-primary btn-md my-0 p pull-right" type="submit">Add to cart
                  <i class="fa fa-shopping-cart ml-1"></i>
                </button>
              </div>
            </div>
            </form>

          </div>
          <!--Content-->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <hr>

      <!--Grid row-->
      <div class="row d-flex justify-content-center wow fadeIn" style="margin-bottom: 180px;">



      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->
  <script type="text/javascript">
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  </script>
