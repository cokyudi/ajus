  <!--Carousel Wrapper-->
  <div id="carousel-example-1z" class="carousel slide carousel-fade pt-4" data-ride="carousel">

    <!--Indicators-->
    <!-- <ol class="carousel-indicators">
      <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-1z" data-slide-to="1"></li>
      <li data-target="#carousel-example-1z" data-slide-to="2"></li>
    </ol> -->
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

      <!--First slide-->
      <div class="carousel-item active">
        <div class="view" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/8-col/img%282%29.jpg'); background-repeat: no-repeat; background-size: cover;">

          <!-- Mask & flexbox options-->
          <div class="mask rgba-black-strong d-flex justify-content-center align-items-center">

            <!-- Content -->
            <div class="text-center white-text mx-5 wow fadeIn">
              <h1 class="mb-4">
                <strong>E-Organizer</strong>
              </h1>

              <p>
                <strong>Event Organizer Online</strong>
              </p>

            </div>
            <!-- Content -->

          </div>
          <!-- Mask & flexbox options-->

        </div>
      </div>
      <!--/First slide-->

    </div>
    <!--/.Slides-->

    <!--Controls-->
    <!-- <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a> -->
    <!--/.Controls-->

  </div>
  <!--/.Carousel Wrapper-->

  <!--Main layout-->
  <main>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark mdb-color darken-3 mb-5">

      <!-- Navbar brand -->
      <span class="navbar-brand">Kategori:</span>

      <!-- Collapse button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">

      <!-- Links -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?=$aktif=='paket'?'active':'';?>">
          <a class="nav-link" href="<?=site_url("")?>">Paket
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <?php foreach ($jenisItems as $jenisItem) { ?>
          <li class="nav-item <?=$aktif==$jenisItem->id_jenis_item?'active':'';?>">
            <a class="nav-link" href="<?=site_url("home/kategori/$jenisItem->id_jenis_item")?>"><?=$jenisItem->jenis_item?></a>
          </li>
        <?php } ?>
        <li class="nav-item <?=$aktif=='promo'?'active':'';?>">
          <a class="nav-link" href="<?=site_url("home/promo")?>">Promo Paket
            <span class="sr-only">(current)</span>
          </a>
        </li>
         <li class="nav-item <?=$aktif=='promoitem'?'active':'';?>">
          <a class="nav-link" href="<?=site_url("home/promoitem")?>">Promo Item
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item <?=$aktif=='upacara'?'active':'';?>">
          <div class="btn-group">
            <a class="nav-link rounded waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Upacara
            </a>
            <div class="dropdown-menu">
              <?php foreach ($upacara as $key => $u): ?>
                <a class="dropdown-item" href="<?=site_url('home/upacara/'.$u->id_upacara)?>">
                  <?= $u->nama_upacara ?>
                </a>
              <?php endforeach ?>
            </div>
          </div>
        </li>

      </ul>
      <!-- Links -->

      <form class="form-inline" method="get" action="<?= site_url('home/search') ?>">
        <div class="md-form my-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search" name="search" aria-label="Search">
        </div>
      </form>
    </div>
    <!-- Collapsible content -->

  </nav>
  <!--/.Navbar-->
    <div class="container">

<?php if($aktif=='paket'){ ?>
            <!--Section: Products v.3-->
      <section class="text-center mb-4">
        <!--Grid row-->
        <div class="row wow fadeIn">
<?php foreach ($items as $item) { ?>
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">
            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img height="200px" src="<?=base_url("assets/images/paket/$item->foto_paket")?>" class="card-img-top" alt="">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->
                <a href="" class="grey-text">
                  <h5></h5>
                </a>
                <h5>
                  <strong>
                    <a href="" class="dark-grey-text" style="font-size:18px"><?=$item->nama_paket;?>
                      <!-- <span class="badge badge-pill danger-color">NEW</span> -->
                    </a>
                  </strong>
                  <div style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;height: 48px;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">
                    <?= $item->nama_upacara ?>
                  </div>
                </h5>

                <h4 class="font-weight-bold blue-text">
                  <strong>Rp. <?=number_format($item->harga_paket,'0','.','.');?></strong>
                </h4>
                <a href="<?=site_url("pesanan/$item->id_paket/1")?>" class="btn btn-indigo">detail</a>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->
          </div>
          <!--Grid column-->
<?php } ?>
        </div>
      <!--Grid row-->
    </section>
    <!--Section: Products v.3-->
<?php } else{?>
        <!--Section: Products v.3-->
      <section class="text-center mb-4">
        <!--Grid row-->
        <div class="row wow fadeIn">
<?php  foreach ($items as $item) { ?>

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">
            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img height="200px" src="<?=base_url("assets/images/item/$item->foto_item")?>" class="card-img-top" alt="">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->
                <a href="" class="grey-text">
                  <h5></h5>
                </a>
                <h5>
                  <div style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;height: 48px;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">
                    <strong><a href="" class="dark-grey-text"><?=$item->nama_item;?></a></strong>
                  </div>
                </h5>

                <h4 class="font-weight-bold blue-text">
                  <strong>Rp. <?=number_format($item->harga_jual,'0','.','.');?></strong>
                </h4>
                <a href="<?=site_url("pesanan/$item->id_item/2")?>" class="btn btn-indigo">detail</a>

              </div>
              <!--Card content-->
            </div>
            <!--Card-->
          </div>
          <!--Grid column-->

<?php } ?>

          </div>
        <!--Grid row-->
      </section>
      <!--Section: Products v.3-->
<?php } ?>


      <!--Pagination-->
      <!-- <nav class="d-flex justify-content-center wow fadeIn">
        <ul class="pagination pg-blue"> -->

          <!--Arrow left-->
          <!-- <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>

          <li class="page-item active">
            <a class="page-link" href="#">1
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">2</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">3</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">4</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">5</a>
          </li>

          <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav> -->
      <!--Pagination-->

    </div>
  </main>
  <!--Main layout-->
